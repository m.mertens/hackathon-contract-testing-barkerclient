import { describe, expect, it } from "bun:test";
import { PactV3, MatchersV3 } from "@pact-foundation/pact";
import BarkClient from "./index";
import path from "path"

const provider = new PactV3({
    dir: path.resolve(process.cwd(), "pacts"),
    consumer: "BarkerAPIClient",
    provider: "BarkerBackend"
})

describe("POST /register", () => {
    it("returns a HTTP 201 and a userId", () => {
        const expectedBody = {
            userId: 1
        }

        provider
            .given("I want to register as a user")
            .uponReceiving("a request for the registration of a user")
            .withRequest({
                method: "POST",
                path: "/register",
                body: {
                    userName: "TestUser",
                    password: "TestPassword"
                },
                headers: { Accept: "application/json" },
            })
            .willRespondWith(
                {
                    status: 201,
                    headers: { "Content-Type": "application/json" },
                    body: MatchersV3.like(expectedBody)
                }
            )

        return provider.executeTest(async (mockserver) => {
            const barkService = new BarkClient(mockserver.url)
            const response = await barkService.createUser({
                userName: "TestUser",
                password: "TestPassword"
            })

            expect(response).toEqual({userId: 1, name: "TestUser"})
        })
    })
})



describe("POST /login", () => {
    it("returns a HTTP 200 and a userId", () => {
        const expectedBody = {
            userId: 1
        }

        provider
            .given("I want to login as a user")
            .uponReceiving("a request for logging in with correct credentials of a user")
            .withRequest({
                method: "POST",
                path: "/login",
                body: {
                    userName: "TestUser",
                    password: "TestPassword"
                },
                headers: { Accept: "application/json" },
            })
            .willRespondWith(
                {
                    status: 200,
                    headers: { "Content-Type": "application/json" },
                    body: MatchersV3.like(expectedBody)
                }
            )

        return provider.executeTest(async (mockserver) => {
            const barkService = new BarkClient(mockserver.url)
            const response = await barkService.loginUser({
                userName: "TestUser",
                password: "TestPassword"
            })

            expect(response).toEqual({userId: 1, name: "TestUser"})
        })
    })

    it("returns a HTTP 401", () => {
        provider
            .given("I want to login as a user")
            .uponReceiving("a request for logging in with incorrectcorrect credentials")
            .withRequest({
                method: "POST",
                path: "/login",
                body: {
                    userName: "TestUser",
                    password: "TestPassword"
                },
                headers: { Accept: "application/json" },
            })
            .willRespondWith(
                {
                    status: 401,
                    headers: { "Content-Type": "application/json" },
                }
            )

        return provider.executeTest(async (mockserver) => {
            const barkService = new BarkClient(mockserver.url)
            const response = await barkService.loginUser({
                userName: "TestUser",
                password: "TestPassword"
            })

            expect(response).toEqual(undefined)
        })
    })
})

describe("POST /bark", () => {
    it("returns a HTTP 201 and a the bark", () => {
        const expectedBody = {
            barkId: 1
        }

        provider
            .given("I want to post a bark")
            .uponReceiving("a request with a bark and the userId")
            .withRequest({
                method: "POST",
                path: "/bark",
                body: {
                    userId: "1",
                    bark: "Testing is so cool! I am sure everybody understands it!"
                },
                headers: { Accept: "application/json" },
            })
            .willRespondWith(
                {
                    status: 201,
                    headers: { "Content-Type": "application/json" },
                    body: MatchersV3.like(expectedBody)
                }
            )

        return provider.executeTest(async (mockserver) => {
            const barkService = new BarkClient(mockserver.url)
            const response = await barkService.postBark({
                userId: "1",
                bark: "Testing is so cool! I am sure everybody understands it!"
            })

            expect(response).toEqual(expectedBody)
        })
    })
})


describe("GET /barks", () => {
    it("returns a HTTP 201 and a the bark", () => {
        const expectedBody = {
            bark: "This Works!",
            timestamp: 1694755032466,
        }

        provider
            .given("I want to read what I have posted")
            .uponReceiving("a request for all the barks that I have made")
            .withRequest({
                method: "GET",
                path: "/barks",
                query: {
                    userId: "1"
                },
                headers: { Accept: "application/json" },
            })
            .willRespondWith(
                {
                    status: 200,
                    headers: { "Content-Type": "application/json" },
                    body: MatchersV3.eachLike(expectedBody)
                }
            )

        return provider.executeTest(async (mockserver) => {
            const barkService = new BarkClient(mockserver.url)
            const response = await barkService.getBarks("1")

            expect(response).toEqual([expectedBody])
        })
    })
})
