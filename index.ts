import axios, { AxiosError } from "axios";

interface User {
    name: string;
    userId: string;
}

interface UserRequest {
    userName: string;
    password: string;
}

interface PostBarkRequest {
    userId: string;
    bark: string;
}

interface Bark {
    username: string,
    timestamp: number,
    bark: string
}

export default class Client {
    baseUrl: string;

    constructor(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    async createUser(userRequest: UserRequest): Promise<User | undefined> {
        const { data, status } = await axios.post(
            this.baseUrl + "/register",
            userRequest,
            { headers: { Accept: "application/json" } }
        )

        if (status != 201) {
            return
        }

        return {
            name: userRequest.userName,
            userId: data.userId,
        }
    }

    async loginUser(userRequest: UserRequest): Promise<User | undefined> {
        try {
            const { data } = await axios.post(
                this.baseUrl + "/login",
                userRequest,
                { headers: { Accept: "application/json" } }
            )

            return {
                name: userRequest.userName,
                userId: data.userId,
            }

        }
        catch (err: AxiosError | any) {
            if (axios.isAxiosError(err)){
                return undefined
            } else {
                throw Error
            }
        }

    }

    async postBark(bark: PostBarkRequest): Promise<any> {
        try {
            const { data } = await axios.post(
                this.baseUrl + "/bark",
                bark,
                { headers: { Accept: "application/json" } }
            )

            return {
                barkId: data.barkId
            }

        }
        catch (err: AxiosError | any) {
            if (axios.isAxiosError(err)){
                return undefined
            } else {
                throw Error
            }
        }

    }

    async getBarks(userId: string): Promise<Bark[] | undefined> {
        try {
            const { data } = await axios.get(
                this.baseUrl + "/barks?userId=" + userId,
                { headers: { Accept: "application/json" } }
            )

            return data as Bark[]
        }
        catch (err: AxiosError | any) {
            if (axios.isAxiosError(err)){
                return undefined
            } else {
                throw Error
            }
        }

    }
}
